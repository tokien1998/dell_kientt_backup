create table if not exists isdn_campaign_s15_tg_mb_2004 
(
isdn string
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
location '/work_zone/adp/vtg/stl/isdn_campaign_s15_tg_mb_2004'
TBLPROPERTIES ('parquet.compression'='snappy');

insert overwrite table isdn_campaign_s15_tg_mb_2004 partition (partition = '${YYYYMMDD}')
select a.* 
from 
	(select isdn, campaign_name 
	from isdn_campaign_s15_ana_tg_mb_2004 
	where partition = '${YYYYMMDD}'

	union
	
	select isdn, campaign_name 
	from isdn_campaign_s15_bau_tg_mb_2004 
	where partition = '${YYYYMMDD}') a
where 
	not exists (
				select substr(b.isdn,4) as isdn 
				from D504_ADP_MO_HIS_VALIDITY b 
				where mon = '${YYMM:DD-1}' 
					and partition = '${YYYYMMDD}' 
					and command = 'S15' 
					and substr(enddate_time,0,8) > '${YYYYMMDD}' 
					and a.isdn = substr(b.isdn,4)
				)
	and not exists (
				select c.isdn 
				from F024_ACCOUNT_BALANCE_MONTH c 
				where country = 'STL' 
				and mon = '${YYMM:DD-1}' 
				and prd_id = '${YYYYMMDD:DD-1}' 
				and ((ACC99_CHARGE+ACC105_CHARGE)*(-1)/1024)>'500' 
				and a.isdn = c.isdn)