--set spark.sql.hive.convertMetastoreParquet=false;
--SET spark.sql.thriftserver.scheduler.pool=long_run;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.parallel=true;
set mapred.reduce.tasks=100;

create table if not exists stl_s15_campaign_2
(isdn string
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
LOCATION '/work_zone/adp/vtg/stl/stl_s15_campaign_2'
TBLPROPERTIES ('parquet.compression'='snappy');

--INSERT OVERWRITE TABLE stl_tmp_recharge_sort PARTITION (partition)
--SELECT * from stl_tmp_recharge_sort_tmp 
--cluster by sub_id;

insert overwrite table stl_s15_campaign_2 partition (partition = '${YYYYMMDD}')
select f.isdn, 'CP_2' campaign_name 
from
	(select isdn
	from F121_tot_charge_accum_final 
	where country = 'STL' AND partition ='20200331'
		and status =2
	and sub_id is not NULL
	and sub_type='MB'
	and upper(product_code) like '%STUDENT%'
	and trim(sub_id) != '') f
inner join 
	(select distinct substring(d.msisdn,4) isdn, count(distinct process_time) sl
	from D501_ADP_VAS_MO_HIS d
	where d.country = 'STL' and mon ='2003' and partition='20200331'
		and  substring(process_time,0,8) >= '20200301'
		and command='S10' and err_code='0'
	group by substring(d.msisdn,4)) a on f.isdn=a.isdn
where sl in (5,6) and b.isdn is null