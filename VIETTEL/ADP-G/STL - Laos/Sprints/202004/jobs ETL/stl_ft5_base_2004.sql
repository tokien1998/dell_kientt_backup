--set spark.sql.hive.convertMetastoreParquet=false;
--SET spark.sql.thriftserver.scheduler.pool=long_run;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.parallel=true;
set mapred.reduce.tasks=100;

create table if not exists stl_ft5_base_2004
(isdn string
,month_actvie string
,v_duration int
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
LOCATION '/work_zone/adp/vtg/stl/stl_ft5_base_2004'
TBLPROPERTIES ('parquet.compression'='snappy');

--INSERT OVERWRITE TABLE stl_tmp_recharge_sort PARTITION (partition)
--SELECT * from stl_tmp_recharge_sort_tmp 
--cluster by sub_id;

insert overwrite table stl_ft5_base_2004 partition (partition = '${YYYYMMDD}')
select
isdn 
,month_actvie 
,v_duration 
,campaign_name 
 from stl_new_subs_dura_under_90 where partition= '${YYYYMMDD}' 