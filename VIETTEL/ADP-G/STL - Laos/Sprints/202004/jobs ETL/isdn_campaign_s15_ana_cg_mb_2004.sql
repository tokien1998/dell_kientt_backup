--set spark.sql.hive.convertMetastoreParquet=false;
--SET spark.sql.thriftserver.scheduler.pool=long_run;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.parallel=true;
set mapred.reduce.tasks=100;

create table if not exists isdn_campaign_s15_ana_cg_mb_2004
(
isdn string,
campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
location '/work_zone/adp&/vtg/stl/isdn_campaign_s15_ana_cg_mb_2004'
TBLPROPERTIES ('parquet.compression'='snappy');


insert overwrite table isdn_campaign_s15_ana_cg_mb_2004 partition (partition = '${YYYYMMDD}')

select a.isdn,campaign_name  
from stl_s15_base_2004 a
inner join stl_recharge_base_ana_cg_mb_2004 b on a.isdn=b.isdn
where partition='${YYYYMMDD}'