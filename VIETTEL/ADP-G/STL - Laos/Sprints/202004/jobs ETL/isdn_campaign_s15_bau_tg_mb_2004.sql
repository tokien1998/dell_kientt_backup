create table if not exists isdn_campaign_s15_bau_tg_mb_2004
(
isdn string
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
location '/work_zone/adp/vtg/stl/isdn_campaign_s15_bau_tg_mb_2004'
TBLPROPERTIES ('parquet.compression'='snappy');

insert overwrite table isdn_campaign_s15_bau_tg_mb_2004 partition (partition = '${YYYYMMDD}')
select 
	tg.isdn
	,'BAU' campaign_name
from stl_recharge_base_bau_tg_mb_2004 tg
inner join 
	(
	select isdn, g_volume
	from F121_tot_charge_accum_final 
	where country = 'STL' and partition ='20200331'
		and g_volume < 5000 
		and status =2 
		and sub_id is not NULL 
		and sub_type='MB' 
		and upper(product_code) like '%STUDENT%' 
		and trim(sub_id) != ''
	) f on tg.isdn=f.isdn
limit 2500;