--set spark.sql.hive.convertMetastoreParquet=false;
--SET spark.sql.thriftserver.scheduler.pool=long_run;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.parallel=true;
set mapred.reduce.tasks=100;

create table if not exists stl_new_subs_dura_under_90
(isdn string
,month_actvie string
,v_duration int
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
LOCATION '/work_zone/adp/vtg/stl/stl_new_subs_dura_under_90'
TBLPROPERTIES ('parquet.compression'='snappy');

--INSERT OVERWRITE TABLE stl_tmp_recharge_sort PARTITION (partition)
--SELECT * from stl_tmp_recharge_sort_tmp 
--cluster by sub_id;

insert overwrite table stl_new_subs_dura_under_90 partition (partition = '${YYYYMMDD}')
select 
isdn
,month_active
,v_duration
,'NEW_SUBS' campaign_name
from
(
select
isdn
,month_active
,v_duration
, rand(1996) as rnd_rank
from F121_tot_charge_accum_final 
where country = 'STL' AND partition ='20200331'
and month_active>='202001' and month_active<='202003'
and v_duration < 5400
and status =2
and sub_id is not NULL
and sub_type='MB'
and trim(sub_id) != ''
union
select
isdn
,month_active
,v_duration
, rand(1996) as rnd_rank
from F121_tot_charge_accum_final 
where country = 'STL' AND partition ='${YYYYMMDD}'
and month_active>='202004' 
and v_duration < 3000
and status =2
and sub_id is not NULL
and sub_type='MB'
and trim(sub_id) != '')