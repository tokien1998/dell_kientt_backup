--set spark.sql.hive.convertMetastoreParquet=false;
--SET spark.sql.thriftserver.scheduler.pool=long_run;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.parallel=true;
set mapred.reduce.tasks=100;

create table if not exists stl_s15_base_2004
(isdn string
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
LOCATION '/work_zone/adp/vtg/stl/stl_s15_base_2004'
TBLPROPERTIES ('parquet.compression'='snappy');


insert overwrite table stl_s15_base_2004 partition (partition = '${YYYYMMDD}')
select isdn ,campaign_name 
from stl_s15_campaign_2 
where partition= '${YYYYMMDD}'