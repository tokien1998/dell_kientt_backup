create table if not exists isdn_campaign_ft5_bau_tg_mb_2004
(
isdn string
,campaign_name string
)
PARTITIONED BY (partition string)
STORED AS PARQUET
location '/work_zone/adp/vtg/stl/isdn_campaign_ft5_bau_tg_mb_2004'
TBLPROPERTIES ('parquet.compression'='snappy');

insert overwrite table isdn_campaign_ft5_bau_tg_mb_2004 partition (partition = '${YYYYMMDD}')
select 
tg.isdn
,'BAU' campaign_name
from stl_recharge_base_bau_tg_mb_2004 tg
limit 20000